﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSPT.Demo.Domain.Entities.ORM
{
    public abstract class BaseEntity
    {
        [Column("LastUpdateDate", TypeName = "datetime")]
        public DateTime? LastUpdateDate { get; set; }

        [Column("CreationDate", TypeName = "datetime")]
        public DateTime CreationDate { get; set; }
    }
}
