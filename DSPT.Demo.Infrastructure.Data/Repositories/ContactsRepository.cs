﻿using System.Collections.Generic;
using System.Linq;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Interfaces.Mapper;
using DSPT.Demo.Domain.Interfaces.ORM;
using DSPT.Demo.Domain.Interfaces.Repositories;

namespace DSPT.Demo.Infrastructure.Data.Repositories
{
    public class ContactsRepository : IContactsRepository
    {
        private readonly IApplicationContext _applicationContext;

        private readonly IContactMapper _contactMapper;

        public ContactsRepository(
            IApplicationContext applicationContext,
            IContactMapper contactMapper)
        {
            this._applicationContext = applicationContext;
            this._contactMapper = contactMapper;
        }

        public IEnumerable<Contact> GetContacts()
        {
            var dbContacts = this._applicationContext.Contacts.ToList();

            return this._contactMapper.GetContacts(dbContacts);
        }
    }
}
