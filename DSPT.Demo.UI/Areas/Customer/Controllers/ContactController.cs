﻿using System.Web.Mvc;
using DSPT.Demo.Services.Interfaces.Presentation;

namespace DSPT.Demo.UI.Areas.Customer.Controllers
{
    public class ContactController : Controller
    {
        public const string IndexViewName = "Index";

        private readonly IContactsPresentationService _contactsPresentationService;

        public ContactController(IContactsPresentationService contactsPresentationService)
        {
            this._contactsPresentationService = contactsPresentationService;
        }

        public ActionResult Index()
        {
            var viewModel = this._contactsPresentationService.GetContactsViewModel();

            return this.View(IndexViewName, viewModel);
        }
    }
}