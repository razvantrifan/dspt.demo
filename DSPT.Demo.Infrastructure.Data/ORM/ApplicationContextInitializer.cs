﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using DSPT.Demo.Infrastructure.Data.Seed;

namespace DSPT.Demo.Infrastructure.Data.ORM
{
    internal class ApplicationContextInitializer : CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            this.SeedContacts(context);
            this.SeedInteractions(context);
        }

        private void SeedContacts(ApplicationContext context)
        {
            var seedContacts = ContactSeedProvider.GetSeedContacts();

            foreach (var seedContact in seedContacts)
            {
                context.Contacts.AddOrUpdate(seedContact);
            }
        }

        private void SeedInteractions(ApplicationContext context)
        {
            var seedInteractions = InteractionSeedProvider.GetSeedInteractions();

            foreach (var seedInteraction in seedInteractions)
            {
                context.Interactions.AddOrUpdate(seedInteraction);
            }
        }
    }
}
