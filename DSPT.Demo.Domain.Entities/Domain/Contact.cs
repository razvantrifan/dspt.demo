﻿using System;

namespace DSPT.Demo.Domain.Entities.Domain
{
    public class Contact
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string Postcode { get; set; }

        public string MobileTelephoneNumber { get; set; }

        public string HomeTelephoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
