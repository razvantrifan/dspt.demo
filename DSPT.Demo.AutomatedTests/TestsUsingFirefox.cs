﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace DSPT.Demo.AutomatedTests
{
    public abstract class TestsUsingFirefox
    {
        protected virtual void ClearBrowserCache(FirefoxDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            Thread.Sleep(5000); //wait 5 seconds to clear cookies.
        }

        protected virtual void WaitUntilPageIsLoaded(FirefoxDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(drv => drv.FindElement(By.Id("pageFooter")));
        }
    }
}
