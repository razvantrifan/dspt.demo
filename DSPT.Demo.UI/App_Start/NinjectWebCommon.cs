[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DSPT.Demo.UI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DSPT.Demo.UI.App_Start.NinjectWebCommon), "Stop")]

namespace DSPT.Demo.UI.App_Start
{
    using System;
    using System.Web;
    using DSPT.Demo.Domain.Interfaces.Mapper;
    using DSPT.Demo.Domain.Interfaces.ORM;
    using DSPT.Demo.Domain.Interfaces.Repositories;
    using DSPT.Demo.Infrastructure.Data.Mapper;
    using DSPT.Demo.Infrastructure.Data.ORM;
    using DSPT.Demo.Infrastructure.Data.Repositories;
    using DSPT.Demo.Service.Presentation;
    using DSPT.Demo.Services.Interfaces.Presentation;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterInfrastructure(kernel);
                RegisterServices(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterInfrastructure(IKernel kernel)
        {
            kernel.Bind<IApplicationContext>().To<ApplicationContext>();
            kernel.Bind<IContactsRepository>().To<ContactsRepository>();
            kernel.Bind<IContactMapper>().To<ContactMapper>();
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IContactsPresentationService>().To<ContactsPresentationService>();
        }
    }
}
