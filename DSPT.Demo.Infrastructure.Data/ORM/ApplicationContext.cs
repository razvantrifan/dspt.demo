﻿using System.Data.Entity;
using DSPT.Demo.Domain.Entities.ORM;
using DSPT.Demo.Domain.Interfaces.ORM;

namespace DSPT.Demo.Infrastructure.Data.ORM
{
    public class ApplicationContext : DbContext, IApplicationContext
    {
        public ApplicationContext() : base("dsptConnString")
        {
            Database.SetInitializer(new ApplicationContextInitializer());
        }

        public DbSet<ContactEntity> Contacts { get; set; }

        public DbSet<InteractionEntity> Interactions { get; set; }
    }
}
