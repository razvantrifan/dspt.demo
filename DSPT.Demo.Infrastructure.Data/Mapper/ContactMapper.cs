﻿using System.Collections.Generic;
using AutoMapper;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Entities.ORM;
using DSPT.Demo.Domain.Interfaces.Mapper;
using DSPT.Demo.Infrastructure.Data.Providers;

namespace DSPT.Demo.Infrastructure.Data.Mapper
{
    public class ContactMapper : IContactMapper
    {
        private readonly IMapper _mapper;

        public ContactMapper()
        {
            this._mapper = AutoMapperProvider.GetMapper();
        }

        public IEnumerable<Contact> GetContacts(IEnumerable<ContactEntity> dbContacts)
        {
            var contacts = new List<Contact>();

            foreach (var dbContact in dbContacts)
            {
                contacts.Add(this.GetContact(dbContact));
            }

            return contacts;
        }

        public Contact GetContact(ContactEntity dbContact)
        {
            return this._mapper.Map<ContactEntity, Contact>(dbContact);
        }
    }
}
