﻿using System.Web.Mvc;

namespace DSPT.Demo.UI.Areas.Reporting
{
    public class ReportingAreaRegistration : AreaRegistration
    {
        public const string ReportingDefaultRouteName = "Reporting_Dashboard";

        public override string AreaName => "Reporting";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                ReportingDefaultRouteName,
                "Reporting/Dashboard",
                new { controller = "Dashboard", action = "Dashboard1", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Reporting_default",
                "Reporting/{controller}/{action}/{id}",
                new { action = "Dashboard1", id = UrlParameter.Optional }
            );
        }
    }
}