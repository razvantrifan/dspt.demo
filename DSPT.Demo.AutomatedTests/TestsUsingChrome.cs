﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace DSPT.Demo.AutomatedTests
{
    public abstract class TestsUsingChrome
    {
        protected virtual void ClearBrowserCache(ChromeDriver driver)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            Thread.Sleep(5000); //wait 5 seconds to clear cookies.
        }

        protected virtual void WaitUntilPageIsLoaded(ChromeDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(drv => drv.FindElement(By.Id("pageFooter")));
        }
    }
}
