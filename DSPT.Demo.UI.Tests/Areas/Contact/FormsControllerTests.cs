﻿using System.Web.Mvc;
using DSPT.Demo.UI.Areas.Forms.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSPT.Demo.UI.Tests.Areas.Contact
{
    [TestClass]
    public class FormsControllerTests
    {
        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheGeneralFormAction_ThenItIsNotNull()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.GeneralForm();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheGeneralFormAction_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.GeneralForm() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(FormsController.GeneralFormViewName, sut.ViewName);
        }

        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheFormValidationAction_ThenItIsNotNull()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.FormValidation();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheFormValidationAction_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.FormValidation() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(FormsController.FormValidationViewName, sut.ViewName);
        }

        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheFormWizzardAction_ThenItIsNotNull()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.FormWizzard();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheFormsController_WhenCallingTheFormWizzardAction_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var formsController = new FormsController();

            // act
            var sut = formsController.FormWizzard() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(FormsController.FormWizzardViewName, sut.ViewName);
        }
    }
}