﻿namespace DSPT.Demo.AutomatedTests.Settings
{
    public class AppSettings
    {
        public string WebsiteUrl { get; set; }

        public string ReportingDashboardUrl { get; set; }
    }
}
