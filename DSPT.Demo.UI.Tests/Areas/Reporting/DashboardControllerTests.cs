﻿using System.Web.Mvc;
using DSPT.Demo.UI.Areas.Reporting.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSPT.Demo.UI.Tests.Areas.Reporting
{
    [TestClass]
    public class DashboardControllerTests
    {
        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard1Action_ThenItIsNotNull()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard1();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard1Action_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard1() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(DashboardController.Dashboard1ViewName, sut.ViewName);
        }

        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard2Action_ThenItIsNotNull()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard2();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard2Action_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard2() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(DashboardController.Dashboard2ViewName, sut.ViewName);
        }

        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard3Action_ThenItIsNotNull()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard3();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheDashboardController_WhenCallingTheDashboard3Action_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var dashboardController = new DashboardController();

            // act
            var sut = dashboardController.Dashboard3() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(DashboardController.Dashboard3ViewName, sut.ViewName);
        }
    }
}