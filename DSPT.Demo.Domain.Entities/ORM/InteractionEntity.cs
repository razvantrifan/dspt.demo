﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSPT.Demo.Domain.Entities.ORM
{
    [Table("Interactions")]
    public class InteractionEntity : BaseEntity
    {
        [Key]
        [Index]
        [Column("InteractionID", TypeName = "bigint")]
        public long InteractionId { get; set; }

        [ForeignKey("Contact")]
        [Column("ContactID", TypeName = "bigint")]
        public long ContactId { get; set; }

        public ContactEntity Contact { get; set; }

        [Column("CampaignID", TypeName = "uniqueidentifier")]
        public Guid CampaignId { get; set; }

        [Column("EngagementValue", TypeName = "decimal")]
        public decimal EngagementValue { get; set; }

        [Column("InteractionStartTime", TypeName = "datetime")]
        public DateTime InteractionStartTime { get; set; }

        [Column("InteractionEndTime", TypeName = "datetime")]
        public DateTime InteractionEndTime { get; set; }

    }
}
