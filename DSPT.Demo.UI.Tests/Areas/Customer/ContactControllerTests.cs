﻿using System.Web.Mvc;
using DSPT.Demo.Domain.Entities.ViewModels.Customer;
using DSPT.Demo.Services.Interfaces.Presentation;
using DSPT.Demo.UI.Areas.Customer.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DSPT.Demo.UI.Tests.Areas.Customer
{
    [TestClass]
    public class ContactControllerTests
    {
        [TestMethod]
        public void GivenTheContactController_WhenCallingTheIndexAction_ThenItIsNotNull()
        {
            // arrange
            var mock = new Mock<IContactsPresentationService>();
            mock.Setup(x => x.GetContactsViewModel()).Returns(new ContactsViewModel());
            var formsController = new ContactController(mock.Object);

            // act
            var sut = formsController.Index();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenTheContactController_WhenCallingTheIndexAction_ThenInReturnsTheCorrectViewResult()
        {
            // arrange
            var mock = new Mock<IContactsPresentationService>();
            mock.Setup(x => x.GetContactsViewModel()).Returns(new ContactsViewModel());
            var formsController = new ContactController(mock.Object);

            // act
            var sut = formsController.Index() as ViewResult;

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(ContactController.IndexViewName, sut.ViewName);
        }
    }
}