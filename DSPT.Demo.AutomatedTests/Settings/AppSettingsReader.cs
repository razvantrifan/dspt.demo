﻿using System.Configuration;

namespace DSPT.Demo.AutomatedTests.Settings
{
    public static class AppSettingsReader
    {
        public static AppSettings GetAppSettings()
        {
            var appSettings = new AppSettings
            {
                WebsiteUrl = ConfigurationManager.AppSettings["DSPT:WebsiteUrl"],
                ReportingDashboardUrl = ConfigurationManager.AppSettings["DSPT:ReportingDashboardUrl"]
            };

            return appSettings;
        }
    }
}
