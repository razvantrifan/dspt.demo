﻿using AutoMapper;

namespace DSPT.Demo.Infrastructure.Data.Providers
{
    public static class AutoMapperProvider
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //cfg.CreateMap()...
                //cfg.AddProfile()... etc...
            });

            return config.CreateMapper();
        }
    }
}
