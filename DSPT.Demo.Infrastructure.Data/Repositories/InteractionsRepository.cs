﻿using System.Collections.Generic;
using System.Linq;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Interfaces.Mapper;
using DSPT.Demo.Domain.Interfaces.ORM;
using DSPT.Demo.Domain.Interfaces.Repositories;

namespace DSPT.Demo.Infrastructure.Data.Repositories
{
    public class InteractionsRepository : IInteractionsRepository
    {
        private readonly IApplicationContext _applicationContext;

        private readonly IInteractionMapper _interactionMapper;

        public InteractionsRepository(
            IApplicationContext applicationContext,
            IInteractionMapper interactionMapper)
        {
            this._applicationContext = applicationContext;
            this._interactionMapper = interactionMapper;
        }

        public IEnumerable<Interaction> GetInteractions()
        {
            var dbInteractions = this._applicationContext.Interactions.ToList();

            return this._interactionMapper.GetInteractions(dbInteractions);
        }
    }
}
