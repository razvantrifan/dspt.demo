﻿using System;

namespace DSPT.Demo.Domain.Entities.Domain
{
    public class Interaction
    {
        public long InteractionId { get; set; }

        public long ContactId { get; set; }

        public Guid CampaignId { get; set; }

        public decimal EngagementValue { get; set; }

        public DateTime InteractionStartTime { get; set; }

        public DateTime InteractionEndTime { get; set; }
    }
}
