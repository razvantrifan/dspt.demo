﻿using System.Collections.Generic;
using AutoMapper;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Entities.ORM;
using DSPT.Demo.Domain.Interfaces.Mapper;
using DSPT.Demo.Infrastructure.Data.Providers;

namespace DSPT.Demo.Infrastructure.Data.Mapper
{
    public class InteractionMapper : IInteractionMapper
    {
        private readonly IMapper _mapper;

        public InteractionMapper()
        {
            this._mapper = AutoMapperProvider.GetMapper();
        }

        public IEnumerable<Interaction> GetInteractions(IEnumerable<InteractionEntity> dbInteractions)
        {
            var interactions = new List<Interaction>();

            foreach (var dbContact in dbInteractions)
            {
                interactions.Add(this.GetInteraction(dbContact));
            }

            return interactions;
        }

        public Interaction GetInteraction(InteractionEntity dbInteraction)
        {
            return this._mapper.Map<InteractionEntity, Interaction>(dbInteraction);
        }
    }
}
