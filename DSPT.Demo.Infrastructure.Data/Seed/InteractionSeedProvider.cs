﻿using System;
using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.ORM;

namespace DSPT.Demo.Infrastructure.Data.Seed
{
    public static class InteractionSeedProvider
    {
        public static IEnumerable<InteractionEntity> GetSeedInteractions()
        {
            return new[]
            {
                new InteractionEntity
                {
                    CampaignId = Guid.Empty,
                    ContactId = 1,
                    EngagementValue = 0,
                    InteractionEndTime = DateTime.Now.AddHours(-2),
                    InteractionStartTime = DateTime.Now.AddHours(-3),
                    LastUpdateDate = DateTime.Now,
                    CreationDate = DateTime.Now
                },
                new InteractionEntity
                {
                    CampaignId = Guid.Empty,
                    ContactId = 1,
                    EngagementValue = 5,
                    InteractionEndTime = DateTime.Now.AddHours(-8),
                    InteractionStartTime = DateTime.Now.AddHours(-9),
                    LastUpdateDate = DateTime.Now,
                    CreationDate = DateTime.Now
                },
                new InteractionEntity
                {
                    CampaignId = Guid.Empty,
                    ContactId = 1,
                    EngagementValue = 15,
                    InteractionEndTime = DateTime.Now.AddHours(-12),
                    InteractionStartTime = DateTime.Now.AddHours(-15),
                    LastUpdateDate = DateTime.Now,
                    CreationDate = DateTime.Now
                },
                new InteractionEntity
                {
                    CampaignId = Guid.Empty,
                    ContactId = 2,
                    EngagementValue = 0,
                    InteractionEndTime = DateTime.Now.AddHours(-2),
                    InteractionStartTime = DateTime.Now.AddHours(-3),
                    LastUpdateDate = DateTime.Now,
                    CreationDate = DateTime.Now
                },
                new InteractionEntity
                {
                    CampaignId = Guid.Empty,
                    ContactId = 3,
                    EngagementValue = 0,
                    InteractionEndTime = DateTime.Now.AddHours(-2),
                    InteractionStartTime = DateTime.Now.AddHours(-3),
                    LastUpdateDate = DateTime.Now,
                    CreationDate = DateTime.Now
                }
            };
        }
    }
}
