﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DSPT.Demo.Domain.Entities.ORM;
using DSPT.Demo.Domain.Interfaces.ORM;
using DSPT.Demo.Infrastructure.Data.Mapper;
using DSPT.Demo.Infrastructure.Data.Repositories;
using DSPT.Demo.Infrastructure.Data.Seed;
using DSPT.Demo.Infrastructure.Data.Tests.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DSPT.Demo.Infrastructure.Data.Tests.Repositories
{
    [TestClass]
    public class ContactsRepositoryTests
    {
        [TestMethod]
        public void GivenAnEmptyListOfDatabaseContacts_WhenGettingTheContactsFromTheRepository_ThenItReturnsTheZeroContacts()
        {
            // arrange
            var mockApplicationContext = new Mock<IApplicationContext>();
            mockApplicationContext.Setup(x => x.Contacts).Returns(new FakeDbSet<ContactEntity>());
            var contactsRepository = new ContactsRepository(mockApplicationContext.Object, new ContactMapper());

            // act
            var sut = contactsRepository.GetContacts();

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(0, sut.Count());
        }

        [TestMethod]
        public void GivenAListOfDatabaseContacts_WhenGettingTheContactsFromTheRepository_ThenItReturnsTheCorrectNumberOfContacts()
        {
            // arrange
            var mockApplicationContext = new Mock<IApplicationContext>();
            var dbContacts = ContactSeedProvider.GetSeedContacts();
            mockApplicationContext.Setup(x => x.Contacts).Returns(GetFakeContacts(dbContacts));
            var contactsRepository = new ContactsRepository(mockApplicationContext.Object, new ContactMapper());

            // act
            var sut = contactsRepository.GetContacts();

            // assert
            Assert.IsNotNull(sut);
            Assert.AreEqual(dbContacts.Count(), sut.Count());
        }

        private static DbSet<ContactEntity> GetFakeContacts(IEnumerable<ContactEntity> dbContacts)
        {
            var contacts = new FakeDbSet<ContactEntity>();
            foreach (var dbContact in dbContacts)
            {
                contacts.Add(dbContact);
            }

            return contacts;
        }
    }
}
