﻿using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Entities.ORM;

namespace DSPT.Demo.Domain.Interfaces.Mapper
{
    public interface IContactMapper
    {
        IEnumerable<Contact> GetContacts(IEnumerable<ContactEntity> dbContacts);

        Contact GetContact(ContactEntity dbContact);
    }
}
