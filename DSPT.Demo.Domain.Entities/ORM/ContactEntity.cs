﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DSPT.Demo.Domain.Entities.ORM
{
    [Table("Contacts")]
    public class ContactEntity : BaseEntity
    {
        [Key]
        [Index]
        [Column("ContactID", TypeName = "bigint")]
        public long ContactId { get; set; }

        [Column("Title", TypeName = "nvarchar")]
        [MaxLength(20)]
        public string Title { get; set; }

        [Column("FirstName", TypeName = "nvarchar")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Column("LastName", TypeName = "nvarchar")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Column("AddressLine1", TypeName = "nvarchar")]
        [MaxLength(100)]
        public string AddressLine1 { get; set; }

        [Column("AddressLine2", TypeName = "nvarchar")]
        [MaxLength(100)]
        public string AddressLine2 { get; set; }

        [Column("Town", TypeName = "nvarchar")]
        [MaxLength(100)]
        public string Town { get; set; }

        [Column("Postcode", TypeName = "nvarchar")]
        [MaxLength(10)]
        public string Postcode { get; set; }

        [Column("MobileTelephoneNumber", TypeName = "nvarchar")]
        [MaxLength(15)]
        public string MobileTelephoneNumber { get; set; }

        [Column("HomeTelephoneNumber", TypeName = "nvarchar")]
        [MaxLength(15)]
        public string HomeTelephoneNumber { get; set; }

        [Column("EmailAddress", TypeName = "nvarchar")]
        [MaxLength(100)]
        public string EmailAddress { get; set; }

        [Column("DateOfBirth", TypeName = "datetime")]
        public DateTime? DateOfBirth { get; set; }

        [ForeignKey("Contact")]
        public ICollection<InteractionEntity> Interactions { get; set; }
    }
}
