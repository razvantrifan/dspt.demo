﻿using DSPT.Demo.AutomatedTests.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace DSPT.Demo.AutomatedTests.HomePage
{
    [TestClass]
    public class HomePageTestsUsingFirefox : TestsUsingFirefox
    {
        private AppSettings _appSettings;

        private FirefoxDriver _webDriver;

        [TestInitialize]
        public void TestInitialize()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();
            this._webDriver = new FirefoxDriver(new FirefoxOptions { AcceptInsecureCertificates = true });
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();
            this._webDriver.Close();
            this._webDriver.Dispose();
        }

        [TestMethod]
        public void GivenTheHomePage_WhenItLoads_ThenItRedirectsToTheReportingDashboard()
        {
            this._webDriver.Manage().Window.Maximize();
            this._webDriver.Navigate().GoToUrl(this._appSettings.WebsiteUrl);
            this.WaitUntilPageIsLoaded(this._webDriver);
            Assert.AreEqual(this._appSettings.ReportingDashboardUrl, this._webDriver.Url);
        }

        [TestMethod]
        public void GivenReportingDashboardPage_WhenItLoads_ThenItContainsTheSidebarMenu()
        {
            this._webDriver.Manage().Window.Maximize();
            this._webDriver.Navigate().GoToUrl(this._appSettings.ReportingDashboardUrl);
            this.WaitUntilPageIsLoaded(this._webDriver);

            Assert.IsNotNull(this._webDriver.FindElement(By.Id("sidebar-menu")));
        }
    }
}
