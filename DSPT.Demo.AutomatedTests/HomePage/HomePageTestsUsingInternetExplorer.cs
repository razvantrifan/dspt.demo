﻿using DSPT.Demo.AutomatedTests.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace DSPT.Demo.AutomatedTests.HomePage
{
    [TestClass]
    public class HomePageTestsUsingInternetExplorer : TestsUsingInternetExplorer
    {
        private AppSettings _appSettings;

        private InternetExplorerDriver _webDriver;

        [TestInitialize]
        public void TestInitialize()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();

            // IE doesn't allow bypassing insecure SSL certificates, so we can't pass in the parameter
            this._webDriver = new InternetExplorerDriver();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();
            this._webDriver.Close();
            this._webDriver.Dispose();
        }

        [TestMethod]
        public void GivenTheHomePage_WhenItLoads_ThenItRedirectsToTheReportingDashboard()
        {
            this._webDriver.Manage().Window.Maximize();
            this._webDriver.Navigate().GoToUrl(this._appSettings.WebsiteUrl);
            this.WaitUntilPageIsLoaded(this._webDriver);

            Assert.AreEqual(this._appSettings.ReportingDashboardUrl, this._webDriver.Url);
        }

        [TestMethod]
        public void GivenReportingDashboardPage_WhenItLoads_ThenItContainsTheSidebarMenu()
        {
            this._webDriver.Manage().Window.Maximize();
            this._webDriver.Navigate().GoToUrl(this._appSettings.ReportingDashboardUrl);
            this.WaitUntilPageIsLoaded(this._webDriver);

            Assert.IsNotNull(this._webDriver.FindElement(By.Id("sidebar-menu")));
        }
    }
}
