﻿using DSPT.Demo.Domain.Entities.ViewModels.Customer;

namespace DSPT.Demo.Services.Interfaces.Presentation
{
    public interface IContactsPresentationService
    {
        ContactsViewModel GetContactsViewModel();
    }
}