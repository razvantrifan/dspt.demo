﻿using System;
using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.ORM;

namespace DSPT.Demo.Infrastructure.Data.Seed
{
    public static class ContactSeedProvider
    {
        public static IEnumerable<ContactEntity> GetSeedContacts()
        {
            return new[]
            {
                new ContactEntity
                {
                    Title = "Mr.",
                    FirstName = "Tom",
                    LastName = "Hanks",
                    DateOfBirth = new DateTime(1980, 1, 15),
                    CreationDate = DateTime.Now
                },
                new ContactEntity
                {
                    Title = "Mr.",
                    FirstName = "Sheriff",
                    LastName = "Woody",
                    DateOfBirth = new DateTime(1975, 6, 20),
                    CreationDate = DateTime.Now
                },
                new ContactEntity
                {
                    Title = "Mr.",
                    FirstName = "Chuck",
                    LastName = "Noland",
                    DateOfBirth = new DateTime(1970, 9, 25),
                    CreationDate = DateTime.Now
                },
                new ContactEntity
                {
                    Title = "Lieutenant",
                    FirstName = "John",
                    LastName = "Miller",
                    DateOfBirth = new DateTime(1985, 3, 5),
                    CreationDate = DateTime.Now
                },
                new ContactEntity
                {
                    Title = "Mr.",
                    FirstName = "Paul",
                    LastName = "Edgecomb",
                    DateOfBirth = new DateTime(1965, 12, 31),
                    CreationDate = DateTime.Now
                }
            };
        }
    }
}
