﻿using System.Web.Mvc;

namespace DSPT.Demo.UI.Areas.Reporting.Controllers
{
    public class DashboardController : Controller
    {
        public const string Dashboard1ViewName = "Dashboard1";

        public const string Dashboard2ViewName = "Dashboard2";

        public const string Dashboard3ViewName = "Dashboard3";

        public ActionResult Dashboard1()
        {
            return this.View(Dashboard1ViewName);
        }

        public ActionResult Dashboard2()
        {
            return this.View(Dashboard2ViewName);
        }

        public ActionResult Dashboard3()
        {
            return this.View(Dashboard3ViewName);
        }
    }
}