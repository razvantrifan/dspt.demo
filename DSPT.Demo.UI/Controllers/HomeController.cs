﻿using System.Web.Mvc;
using DSPT.Demo.UI.Areas.Reporting;

namespace DSPT.Demo.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return this.RedirectToRoute(ReportingAreaRegistration.ReportingDefaultRouteName);
        }
    }
}