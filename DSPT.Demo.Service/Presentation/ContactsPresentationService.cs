﻿using DSPT.Demo.Domain.Entities.ViewModels.Customer;
using DSPT.Demo.Domain.Interfaces.Repositories;
using DSPT.Demo.Services.Interfaces.Presentation;

namespace DSPT.Demo.Service.Presentation
{
    public class ContactsPresentationService : IContactsPresentationService
    {
        private readonly IContactsRepository _contactsRepository;

        public ContactsPresentationService(IContactsRepository contactsRepository)
        {
            this._contactsRepository = contactsRepository;
        }

        public ContactsViewModel GetContactsViewModel()
        {
            var viewModel = new ContactsViewModel
            {
                Contacts = this._contactsRepository.GetContacts()
            };

            return viewModel;
        }
    }
}
