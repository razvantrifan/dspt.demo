﻿using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.Domain;

namespace DSPT.Demo.Domain.Interfaces.Repositories
{
    public interface IInteractionsRepository
    {
        IEnumerable<Interaction> GetInteractions();
    }
}