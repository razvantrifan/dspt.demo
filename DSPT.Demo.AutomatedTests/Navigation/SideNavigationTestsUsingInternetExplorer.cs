﻿using System.Linq;
using DSPT.Demo.AutomatedTests.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace DSPT.Demo.AutomatedTests.Navigation
{
    [TestClass]
    public class SideNavigationTestsUsingInternetExplorer : TestsUsingInternetExplorer
    {
        private AppSettings _appSettings;

        private InternetExplorerDriver _webDriver;

        [TestInitialize]
        public void TestInitialize()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();
            this._webDriver = new InternetExplorerDriver();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this._appSettings = AppSettingsReader.GetAppSettings();
            this._webDriver.Close();
            this._webDriver.Dispose();
        }

        [TestMethod]
        public void GivenReportingDashboardPage_WhenItLoads_ThenTheSideNavigationContains3DashboardLinks()
        {
            this._webDriver.Manage().Window.Maximize();
            this._webDriver.Navigate().GoToUrl(this._appSettings.ReportingDashboardUrl);
            this.WaitUntilPageIsLoaded(this._webDriver);

            var block = this._webDriver.FindElement(By.Id("sidebar-menu"));

            Assert.IsNotNull(block);

            var menuSections = block.FindElements(By.ClassName("menu_section"));

            Assert.IsNotNull(menuSections);
            Assert.IsTrue(menuSections.Count > 0);

            var sectionSideMenu = menuSections.First().FindElement(By.ClassName("side-menu"));

            Assert.IsNotNull(sectionSideMenu);

            var dashboardListItems = sectionSideMenu.FindElements(By.XPath("li[1]/ul/li/a"));

            Assert.IsNotNull(dashboardListItems);
            Assert.AreEqual(3, dashboardListItems.Count);
        }
    }
}
