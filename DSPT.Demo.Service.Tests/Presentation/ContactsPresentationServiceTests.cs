﻿using System.Collections.Generic;
using System.Linq;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Entities.ORM;
using DSPT.Demo.Domain.Interfaces.Repositories;
using DSPT.Demo.Infrastructure.Data.Mapper;
using DSPT.Demo.Infrastructure.Data.Seed;
using DSPT.Demo.Service.Presentation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DSPT.Demo.Service.Tests.Presentation
{
    [TestClass]
    public class ContactsPresentationServiceTests
    {
        [TestMethod]
        public void GivenAnEmptyContactsRepository_WhenGettingTheContactsViewModel_ThenItsNotNull()
        {
            // arrange
            var mockContactsRepository = new Mock<IContactsRepository>();
            var contactsPresentationService = new ContactsPresentationService(mockContactsRepository.Object);

            // act
            var sut = contactsPresentationService.GetContactsViewModel();

            // assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void GivenAnEmptyContactsRepository_WhenGettingTheContactsViewModel_ThenTheContactsListIsEmpty()
        {
            // arrange
            var mockContactsRepository = new Mock<IContactsRepository>();
            var contactsPresentationService = new ContactsPresentationService(mockContactsRepository.Object);

            // act
            var sut = contactsPresentationService.GetContactsViewModel();

            // assert
            Assert.IsNotNull(sut.Contacts);
            Assert.AreEqual(0, sut.Contacts.Count());
        }

        [TestMethod]
        public void GivenAPopulatedContactsRepository_WhenGettingTheContactsViewModel_ThenTheContactsArePopulated()
        {
            // arrange
            var mockContactsRepository = new Mock<IContactsRepository>();
            var dbContacts = ContactSeedProvider.GetSeedContacts();
            mockContactsRepository.Setup(x => x.GetContacts()).Returns(GetFakeContacts(dbContacts));
            var contactsPresentationService = new ContactsPresentationService(mockContactsRepository.Object);

            // act
            var sut = contactsPresentationService.GetContactsViewModel();

            // assert
            Assert.IsNotNull(sut.Contacts);
            Assert.AreEqual(dbContacts.Count(), sut.Contacts.Count());
        }

        private static IEnumerable<Contact> GetFakeContacts(IEnumerable<ContactEntity> dbContacts)
        {
            var contactMapper = new ContactMapper();
            var contacts = contactMapper.GetContacts(dbContacts);

            return contacts;
        }
    }
}
