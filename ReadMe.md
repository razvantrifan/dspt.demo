# Website Configuration
## Host Configuration
Add an entry to your hosts file C:\Windows\System32\drivers\etc\hosts:
	127.0.0.1		demo.dspt.local

## IIS Configuration
Create an IIS website called demo.dspt.local which runs under C:\inetpub\wwwroot\demo.dspt.local.

## SQL Configuration
The website uses a instance called SQLDEV2016, which is a SQL Server 2016 Development edition. Create an user called 'root' with password 'root' and give it database creation permissions.

## Deploy
Deploy the website locally and run it.

## LESS
LESS uses the VS Web Compiler extension to compile the stylesheets