﻿using System.Web.Mvc;

namespace DSPT.Demo.UI.Areas.Forms.Controllers
{
    public class FormsController : Controller
    {
        public const string GeneralFormViewName = "GeneralForm";

        public const string FormValidationViewName = "FormValidation";

        public const string FormWizzardViewName = "FormWizzard";

        public ActionResult GeneralForm()
        {
            return this.View(GeneralFormViewName);
        }

        public ActionResult FormValidation()
        {
            return this.View(FormValidationViewName);
        }

        public ActionResult FormWizzard()
        {
            return this.View(FormWizzardViewName);
        }
    }
}