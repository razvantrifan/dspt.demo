﻿using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.Domain;

namespace DSPT.Demo.Domain.Entities.ViewModels.Customer
{
    public class ContactsViewModel
    {
        public IEnumerable<Contact> Contacts { get; set; }
    }
}
