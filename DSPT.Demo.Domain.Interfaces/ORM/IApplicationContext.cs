﻿using System.Data.Entity;
using DSPT.Demo.Domain.Entities.ORM;

namespace DSPT.Demo.Domain.Interfaces.ORM
{
    public interface IApplicationContext
    {
        DbSet<ContactEntity> Contacts { get; set; }

        DbSet<InteractionEntity> Interactions { get; set; }
    }
}
