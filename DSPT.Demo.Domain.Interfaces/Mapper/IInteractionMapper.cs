﻿using System.Collections.Generic;
using DSPT.Demo.Domain.Entities.Domain;
using DSPT.Demo.Domain.Entities.ORM;

namespace DSPT.Demo.Domain.Interfaces.Mapper
{
    public interface IInteractionMapper
    {
        IEnumerable<Interaction> GetInteractions(IEnumerable<InteractionEntity> dbInteractions);

        Interaction GetInteraction(InteractionEntity dbInteraction);
    }
}
